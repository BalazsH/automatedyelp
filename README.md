This is a simple case study in automating the generations and rating of yelp reviews.

There is a demo that can be run locally though the following steps

clone repository 
```
git clone https://gitlab.com/BalazsH/automatedyelp
cd automatedyelp
```


Download Model Weights (needed for classification)

```
aws s3 sync s3://yelp-nlp-bh/yelp-distilbert ./
```


Frontend

```
cd yelp-react
yarn install
npm start
```

Backend
```
pip install -r requirements.txt
env FLASK_APP=yelp_api.py flask run --port=8000
```