from __future__ import absolute_import, division, print_function

import logging
import re

import numpy as np
import torch
from pytorch_transformers import (BertConfig,
                                  BertForSequenceClassification, BertTokenizer, BertModel,
                                  XLMConfig, XLMForSequenceClassification,
                                  XLMTokenizer, XLNetConfig,
                                  XLNetForSequenceClassification,
                                  XLNetTokenizer, XLMModel, RobertaModel, RobertaConfig,
                                  RobertaForSequenceClassification, RobertaTokenizer,
                                  DistilBertConfig, DistilBertTokenizer, DistilBertModel)
from pytorch_transformers.modeling_distilbert import DistilBertPreTrainedModel

from pytorch_transformers.modeling_roberta import RobertaClassificationHead
from pytorch_transformers.modeling_utils import SequenceSummary
from torch import nn
from torch.nn import BCEWithLogitsLoss
from .utils import (convert_examples_to_features,
                        InputExample)

class DistilBertForSequenceClassification(DistilBertPreTrainedModel):
    r"""
        **labels**: (`optional`) ``torch.LongTensor`` of shape ``(batch_size,)``:
            Labels for computing the sequence classification/regression loss.
            Indices should be in ``[0, ..., config.num_labels - 1]``.
            If ``config.num_labels == 1`` a regression loss is computed (Mean-Square loss),
            If ``config.num_labels > 1`` a classification loss is computed (Cross-Entropy).
    Outputs: `Tuple` comprising various elements depending on the configuration (config) and inputs:
        **loss**: (`optional`, returned when ``labels`` is provided) ``torch.FloatTensor`` of shape ``(1,)``:
            Classification (or regression if config.num_labels==1) loss.
        **logits**: ``torch.FloatTensor`` of shape ``(batch_size, config.num_labels)``
            Classification (or regression if config.num_labels==1) scores (before SoftMax).
        **hidden_states**: (`optional`, returned when ``config.output_hidden_states=True``)
            list of ``torch.FloatTensor`` (one for the output of each layer + the output of the embeddings)
            of shape ``(batch_size, sequence_length, hidden_size)``:
            Hidden-states of the model at the output of each layer plus the initial embedding outputs.
        **attentions**: (`optional`, returned when ``config.output_attentions=True``)
            list of ``torch.FloatTensor`` (one for each layer) of shape ``(batch_size, num_heads, sequence_length, sequence_length)``:
            Attentions weights after the attention softmax, used to compute the weighted average in the self-attention heads.
    Examples::
        tokenizer = DistilBertTokenizer.from_pretrained('distilbert-base-uncased')
        model = DistilBertForSequenceClassification.from_pretrained('distilbert-base-uncased')
        input_ids = torch.tensor(tokenizer.encode("Hello, my dog is cute")).unsqueeze(0)  # Batch size 1
        labels = torch.tensor([1]).unsqueeze(0)  # Batch size 1
        outputs = model(input_ids, labels=labels)
        loss, logits = outputs[:2]
    """
    def __init__(self, config):
        super(DistilBertForSequenceClassification, self).__init__(config)
        self.num_labels = config.num_labels

        self.distilbert = DistilBertModel(config)
        self.pre_classifier = nn.Linear(config.dim, config.dim)
        self.classifier = nn.Linear(config.dim, config.num_labels)
        # self.regressor = nn.Linear(config.dim, 1)
        self.dropout = nn.Dropout(config.seq_classif_dropout)

        self.init_weights()

    def forward(self, input_ids, attention_mask=None, head_mask=None, labels=None):
        distilbert_output = self.distilbert(input_ids=input_ids,
                                            attention_mask=attention_mask,
                                            head_mask=head_mask)
        hidden_state = distilbert_output[0]  # (bs, seq_len, dim)
        pooled_output = hidden_state[:, 0]  # (bs, dim)
        pooled_output = self.pre_classifier(pooled_output)  # (bs, dim)
        pooled_output = nn.ReLU()(pooled_output)  # (bs, dim)
        pooled_output = self.dropout(pooled_output)  # (bs, dim)

        logits = self.classifier(pooled_output)  # (bs, dim)

        outputs = (logits,) + distilbert_output[1:]
        if labels is not None:
            if self.num_labels == 1:
                loss_fct = nn.MSELoss()
                loss = loss_fct(logits.view(-1), labels.view(-1))
            else:
                loss_fct = nn.CrossEntropyLoss()
                loss = loss_fct(logits.view(-1, self.num_labels), labels-1)
            outputs = (loss,) + outputs

        return outputs  # (loss), logits, (hidden_states), (attentions)


MODEL_CLASSES = {
    "bert": (BertConfig, BertForSequenceClassification, BertTokenizer),
    "xlnet": (XLNetConfig, XLNetForSequenceClassification, XLNetTokenizer),
    "xlm": (XLMConfig, XLMForSequenceClassification, XLMTokenizer),
    "roberta": (RobertaConfig, RobertaForSequenceClassification, RobertaTokenizer),
    "distilbert": (DistilBertConfig, DistilBertForSequenceClassification, DistilBertTokenizer),

}

model_type = "distilbert"
model_name_or_path = "distilbert-base-uncased"
checkpoint = "yelp-distilbert"
task_name = "yelp"
num_labels = 5
config_class, model_class, tokenizer_class = MODEL_CLASSES[model_type]
config = config_class.from_pretrained(checkpoint, num_labels=num_labels, finetuning_task=task_name)
tokenizer = tokenizer_class.from_pretrained(model_name_or_path, do_lower_case=True)
model = model_class.from_pretrained(checkpoint, from_tf=bool('.ckpt' in checkpoint), config=config)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
# device = 'cpu'
model.eval()
model.to(device)
label_list = [1,2,3,4,5]
labels = ["1", "2", "3", "4", "5"]

def predict_messages(messages):


    eval_examples = []
    for input_string in messages:
        eval_examples.append(
            InputExample(guid="123", text_a=input_string, text_b=None, label=1)
        )



    eval_features = convert_examples_to_features(
        eval_examples, label_list, 128, tokenizer, "regression"
    )

    input_ids = torch.tensor([f.input_ids for f in eval_features], dtype=torch.long).to(
        device
    )
    input_mask = torch.tensor(
        [f.input_mask for f in eval_features], dtype=torch.long
    ).to(device)

    inputs = {'input_ids': input_ids,
              'attention_mask': input_mask,
              'labels': None}
    with torch.no_grad():

            logits = model(**inputs)[0]
            logits = torch.sigmoid(logits)
            logits = logits.cpu().detach().numpy().tolist()
            response = np.argmax(logits, axis=1) + 1


    return list(response)