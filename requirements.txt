pytorch-transformers==1.2.0
pandas==0.25.1
numpy==1.17.2
scikit-learn==0.21.3
tqdm==4.36.1
Flask==1.1.1