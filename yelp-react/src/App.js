import React, {Component} from 'react';
import StarRatings from 'react-star-ratings';
import './App.css';



class App extends Component {
    state = {
        text_input: "This restaurant was great! The food ",
        stars: 0
    }


    getSuggestions(event) {


        this.setState({
            loading: true,
            foundNothing: false,
        });

        fetch(`http://localhost:8000/review/?text=${this.state.text_input}`).then(function (response) {
            return response.json();
        })
            .then(jsonStr => {
                console.log(jsonStr);
                this.setState({text_input: jsonStr.text});
                this.setState({stars: jsonStr.stars});

                console.log(jsonStr);
            });

        console.log(this.state.stars)
        console.log(this.state.text_input)

    }

    handleChange = (e) => {
        this.setState({text_input: e.target.value});
    }

    render() {
        return (


            <form onSubmit={(e) => {
                e.preventDefault();
                this.getSuggestions(e);
            }}>
                <label>
                    User Input:
                    <textarea type="text" name="text_input"
                              cols="40" rows="5"
                              value={this.state.text_input}
                              onChange={this.handleChange}/>
                </label>
                <input type="submit" value="Submit"/>
                <br/>
                {/*<text>stars: {this.state.stars}</text>*/}
                <StarRatings
                    rating={this.state.stars}
                    starRatedColor="red"
                    // changeRating={this.changeRating}
                    numberOfStars={5}
                    name='rating'
                />
            </form>
        )
            ;
    }
}

export default App;
