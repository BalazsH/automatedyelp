from flask import Flask, escape, request, jsonify
from nlp.inference import predict_messages
from nlp.generation import generate_text

app = Flask(__name__)

@app.route('/review/', methods=['GET'])
def hello():
    if request.method =='GET':

        text_input = request.args.get("text", '')
        stars = predict_messages([text_input])
        text_output = generate_text(text_input)
        text_output = text_output.strip()  # otherwise we got extra spaces.
        response = jsonify(text=text_output, stars=int(stars[0]))
        # LOCAL ONLY!
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response, 200


if __name__ == "__main__":
    app.run(port=8000)